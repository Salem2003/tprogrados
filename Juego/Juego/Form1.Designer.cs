﻿namespace Juego
{
    partial class frmJuego
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJuego));
            this.btnAgarrar = new System.Windows.Forms.Button();
            this.lblPC = new System.Windows.Forms.Label();
            this.lblJugador = new System.Windows.Forms.Label();
            this.imglistMazo = new System.Windows.Forms.ImageList(this.components);
            this.pcbJugador = new System.Windows.Forms.PictureBox();
            this.pcbPC = new System.Windows.Forms.PictureBox();
            this.pcbMazo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pcbJugador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbMazo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAgarrar
            // 
            this.btnAgarrar.BackColor = System.Drawing.Color.Red;
            this.btnAgarrar.Location = new System.Drawing.Point(319, 367);
            this.btnAgarrar.Name = "btnAgarrar";
            this.btnAgarrar.Size = new System.Drawing.Size(174, 71);
            this.btnAgarrar.TabIndex = 0;
            this.btnAgarrar.Text = "Agarrar";
            this.btnAgarrar.UseVisualStyleBackColor = false;
            this.btnAgarrar.Click += new System.EventHandler(this.btnAgarrar_Click);
            // 
            // lblPC
            // 
            this.lblPC.AutoSize = true;
            this.lblPC.Location = new System.Drawing.Point(110, 340);
            this.lblPC.Name = "lblPC";
            this.lblPC.Size = new System.Drawing.Size(30, 20);
            this.lblPC.TabIndex = 4;
            this.lblPC.Text = "PC";
            // 
            // lblJugador
            // 
            this.lblJugador.AutoSize = true;
            this.lblJugador.Location = new System.Drawing.Point(643, 340);
            this.lblJugador.Name = "lblJugador";
            this.lblJugador.Size = new System.Drawing.Size(67, 20);
            this.lblJugador.TabIndex = 5;
            this.lblJugador.Text = "Jugador";
            // 
            // imglistMazo
            // 
            this.imglistMazo.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imglistMazo.ImageStream")));
            this.imglistMazo.TransparentColor = System.Drawing.Color.Transparent;
            this.imglistMazo.Images.SetKeyName(0, "1 pica.png");
            this.imglistMazo.Images.SetKeyName(1, "2 pica.png");
            this.imglistMazo.Images.SetKeyName(2, "3 pica.png");
            this.imglistMazo.Images.SetKeyName(3, "4 pica.png");
            this.imglistMazo.Images.SetKeyName(4, "5 pica.png");
            this.imglistMazo.Images.SetKeyName(5, "6 pica.png");
            this.imglistMazo.Images.SetKeyName(6, "7 pica.png");
            this.imglistMazo.Images.SetKeyName(7, "8 pica.png");
            this.imglistMazo.Images.SetKeyName(8, "9 pica.png");
            this.imglistMazo.Images.SetKeyName(9, "10 pica.png");
            this.imglistMazo.Images.SetKeyName(10, "J pica.png");
            this.imglistMazo.Images.SetKeyName(11, "Q pica.png");
            this.imglistMazo.Images.SetKeyName(12, "K pica.png");
            this.imglistMazo.Images.SetKeyName(13, "1 diamante.png");
            this.imglistMazo.Images.SetKeyName(14, "2 diamante.png");
            this.imglistMazo.Images.SetKeyName(15, "3 diamante.png");
            this.imglistMazo.Images.SetKeyName(16, "4 diamante.png");
            this.imglistMazo.Images.SetKeyName(17, "5 diamante.png");
            this.imglistMazo.Images.SetKeyName(18, "6 diamante.png");
            this.imglistMazo.Images.SetKeyName(19, "7 diamante.png");
            this.imglistMazo.Images.SetKeyName(20, "8 diamante.png");
            this.imglistMazo.Images.SetKeyName(21, "9 diamante.png");
            this.imglistMazo.Images.SetKeyName(22, "10 diamante.png");
            this.imglistMazo.Images.SetKeyName(23, "J diamante.png");
            this.imglistMazo.Images.SetKeyName(24, "Q diamante.png");
            this.imglistMazo.Images.SetKeyName(25, "K diamante.png");
            this.imglistMazo.Images.SetKeyName(26, "1 trebol.png");
            this.imglistMazo.Images.SetKeyName(27, "2 trebol.png");
            this.imglistMazo.Images.SetKeyName(28, "3 trebol.png");
            this.imglistMazo.Images.SetKeyName(29, "4 trebol.png");
            this.imglistMazo.Images.SetKeyName(30, "5 trebol.png");
            this.imglistMazo.Images.SetKeyName(31, "6 trebol.png");
            this.imglistMazo.Images.SetKeyName(32, "7 trebol.png");
            this.imglistMazo.Images.SetKeyName(33, "8 trebol.png");
            this.imglistMazo.Images.SetKeyName(34, "9 trebol.png");
            this.imglistMazo.Images.SetKeyName(35, "10 trebol.png");
            this.imglistMazo.Images.SetKeyName(36, "J trebol.png");
            this.imglistMazo.Images.SetKeyName(37, "Q trebol.png");
            this.imglistMazo.Images.SetKeyName(38, "K trebol.png");
            this.imglistMazo.Images.SetKeyName(39, "1 corazon.png");
            this.imglistMazo.Images.SetKeyName(40, "2 corazon.png");
            this.imglistMazo.Images.SetKeyName(41, "3 corazon.png");
            this.imglistMazo.Images.SetKeyName(42, "4 corazon.png");
            this.imglistMazo.Images.SetKeyName(43, "5 corazon.png");
            this.imglistMazo.Images.SetKeyName(44, "6 corazon.png");
            this.imglistMazo.Images.SetKeyName(45, "7 corazon.png");
            this.imglistMazo.Images.SetKeyName(46, "8 corazon.png");
            this.imglistMazo.Images.SetKeyName(47, "9 corazon.png");
            this.imglistMazo.Images.SetKeyName(48, "10 corazon.png");
            this.imglistMazo.Images.SetKeyName(49, "J corazon.png");
            this.imglistMazo.Images.SetKeyName(50, "Q corazon.png");
            this.imglistMazo.Images.SetKeyName(51, "K corazon.png");
            // 
            // pcbJugador
            // 
            this.pcbJugador.Image = global::Juego.Properties.Resources._1_diamante;
            this.pcbJugador.Location = new System.Drawing.Point(599, 119);
            this.pcbJugador.Name = "pcbJugador";
            this.pcbJugador.Size = new System.Drawing.Size(143, 189);
            this.pcbJugador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbJugador.TabIndex = 3;
            this.pcbJugador.TabStop = false;
            // 
            // pcbPC
            // 
            this.pcbPC.Image = global::Juego.Properties.Resources._1_diamante;
            this.pcbPC.Location = new System.Drawing.Point(57, 119);
            this.pcbPC.Name = "pcbPC";
            this.pcbPC.Size = new System.Drawing.Size(143, 189);
            this.pcbPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbPC.TabIndex = 2;
            this.pcbPC.TabStop = false;
            // 
            // pcbMazo
            // 
            this.pcbMazo.Image = global::Juego.Properties.Resources.dorso;
            this.pcbMazo.Location = new System.Drawing.Point(293, 105);
            this.pcbMazo.Name = "pcbMazo";
            this.pcbMazo.Size = new System.Drawing.Size(239, 212);
            this.pcbMazo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbMazo.TabIndex = 1;
            this.pcbMazo.TabStop = false;
            // 
            // frmJuego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblJugador);
            this.Controls.Add(this.lblPC);
            this.Controls.Add(this.pcbJugador);
            this.Controls.Add(this.pcbPC);
            this.Controls.Add(this.pcbMazo);
            this.Controls.Add(this.btnAgarrar);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "frmJuego";
            this.Text = "Juego";
            this.Load += new System.EventHandler(this.frmJuego_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcbJugador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbMazo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAgarrar;
        private System.Windows.Forms.PictureBox pcbMazo;
        private System.Windows.Forms.PictureBox pcbPC;
        private System.Windows.Forms.PictureBox pcbJugador;
        private System.Windows.Forms.Label lblPC;
        private System.Windows.Forms.Label lblJugador;
        private System.Windows.Forms.ImageList imglistMazo;
    }
}

