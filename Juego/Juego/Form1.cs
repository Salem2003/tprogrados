﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clases;

namespace Juego
{
    public partial class frmJuego : Form
    {
        Mazo mazo;

        public frmJuego()
        {
            InitializeComponent();
        }

        private void frmJuego_Load(object sender, EventArgs e)
        {
            mazo = new Mazo();
            mazo.Generar();
        }

        private void btnAgarrar_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int cont = 0;

            //for (int i = 0; i < mazo.cartas.Count; i++)
            //{
            if (cont < mazo.cartas.Count)
            {
                int i = 0;

                i = random.Next(0, mazo.cartas.Count);

                if (mazo.cartas[i].yaSalio == false)
                {
                    pcbJugador.Image = imglistMazo.Images[i];
                    pcbJugador.SizeMode = PictureBoxSizeMode.StretchImage;
                    mazo.cartas[i].yaSalio = true;
                }

                i = random.Next(0, mazo.cartas.Count);

                if (mazo.cartas[i].yaSalio == false)
                {
                    pcbPC.Image = imglistMazo.Images[i];
                    pcbPC.SizeMode = PictureBoxSizeMode.StretchImage;
                    mazo.cartas[i].yaSalio = true;
                }

                i++;
                cont++;
            }
            //}
        }
    }
}
