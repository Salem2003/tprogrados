﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clases
{
    public class Carta
    {
        public enum enumPalo //SE PUEDE CAMBIAR A TIPO DE CARTA (CAMPEON,EJERCITO, ETC)
        {
            pica,
            diamante,
            trebol,
            corazon
        }

        public int numero { get; set; }
        public enumPalo palo { get; set; }
        public int imagen { get; set; } //LISTA CON TODAS LAS CARTAS, Y CADA UNA CON SU POSICION (IMAGINARSE COMO, EL 1 DE BASTO ES LA CARTA 54)

        public bool yaSalio = false;

        public override string ToString()
        {
            string msg = "Vacio";
            if (numero != 0) //Si el numero
            {
                msg = palo.ToString() + "," + "" + numero.ToString(); //ENTONCES CUANDO LE PREGUNTE, ME VA A DECIR QUE ES (ESPADA 4, BASTO 2, ETC)
            }

            return msg;
        }
    }
}
